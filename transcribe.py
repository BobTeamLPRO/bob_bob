#!/usr/bin/env python
from os import environ, path

from pocketsphinx.pocketsphinx import *
from sphinxbase.sphinxbase import *

MODELDIR = "pocketsphinx/model"
DATADIR = "pocketsphinx/test/data"

def decode(archivo):
	# Create a decoder with certain model
	config = Decoder.default_config()
	config.set_string('-hmm', 'modelo/hmm')
	config.set_string('-lm', 'modelo/bob.lm')
	config.set_string('-dict', 'modelo/bob.dic')
	decoder = Decoder(config)

	# Decode streaming data.
	decoder = Decoder(config)
	decoder.start_utt()
	stream = open(archivo, 'rb')
	while True:
	  buf = stream.read(1024)
	  if buf:
	    decoder.process_raw(buf, False, False)
	  else:
	    break
	decoder.end_utt()


	ret = []
	for best, i in zip(decoder.nbest(),range(10)):
		if type(best.hypstr) != None:
			print( best.hypstr, best.score)
			ret.append(best.hypstr)

	return ret