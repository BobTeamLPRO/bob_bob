import urllib2
import os
import sys
import json
import httplib
import wave
import subprocess
import codecs
from time import sleep

GOOGLE_SPEECH_URL_V2 = "https://www.google.com/speech-api/v2/recognize?output=json&lang=es-ES&key=AIzaSyCeKMAYMNFpuCtt2wg9ssz0mzAGmZol2s0";

def extract_posibles_phrases(response):
	res = len(json.loads(response)['result'][0]['alternative'])
	ret = []
	for x in xrange(0,4):
		ret.append(json.loads(response)['result'][0]['alternative'][x]['transcript'])
	return ret


#envia a Google Speech Api el archivo (.flac) que se pasa por parametro
# y devuelve posibles frases (la mas probable siempre es la 1)
def stt_google(filename):
	f = open(filename, 'rb')
 	flac_cont = f.read()
 	f.close()
 
	# Cabecera de la peticion
	hrs = {"User-Agent": "Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.63 Safari/535.7",
	'Content-type': 'audio/x-flac; rate=16000'} 
	
	req = urllib2.Request(GOOGLE_SPEECH_URL_V2, data=flac_cont, headers=hrs)
	#print "Sending request to Google TTS"
	p = urllib2.urlopen(req)
	response = p.read()
	response = response.split('\n', 1)[1]

	if response.strip() == '':
		return "repitelo por favor, no te he entendido"
	# Try to get something out of the complicated json response:
	else:
		res = extract_posibles_phrases(response)
		return res
	