#!/usr/bin/env python
# -*- coding: utf-8 -*-
from socket import *
import sys
import select
import subprocess
import fcntl
import struct
#import speech_api
import threading
import transcribe
#import ispeech
import pruebanuance
import wave

import unicodedata

ACTIVE = False
MODONOCHE = False
IP_ANDROID = ''
IP_MINIBOB = []

MINIBOB_ACTIVO = ''
PORT_ANDROID = 9999

MUSICA = False
FASE = 1

EMAIL = False
FASE_EMAIL = 1

CANCION = "archivo"

IFNAME = "wlan0"


def elimina_tildes(cadena):
    s = ''.join((c for c in unicodedata.normalize('NFD',unicode(cadena)) if unicodedata.category(c) != 'Mn'))
    return s.decode()

def enviar_android(texto):
	try:
		s = socket(AF_INET, SOCK_STREAM)
		s.connect((IP_ANDROID, PORT_ANDROID))
		s.sendall(texto)
		#data = s.recv(1024)
		s.close()
	finally:
		pass

def sendToMinibob(ip,texto):
	try:
		addr = (ip,9998)
		s = socket(AF_INET,SOCK_STREAM)
		s.connect(addr)
		s.sendall(texto)
		s.close()
	finally:
		print "se intento enviar a " + ip
		pass


def activar_modo_noche():
	global MODONOCHE
	MODONOCHE = True
	return "modo noche activado"

def desactivar_modo_noche():
	global MODONOCHE
	MODONOCHE = False
	return "modo noche desactivado"

def emergencia():
	enviar_android('emergencia')
	return "no te preocupes estoy avisando a alguien"

def error():
	return ""

def musica():
	global MUSICA
	global FASE
	FASE = 1
	MUSICA = True
	return "que cancion quiere escuchar?"

def enviar_correo():
	global EMAIL
	global FASE_EMAIL
	FASE_EMAIL = 1
	EMAIL = True
	return "¿cual es el destinatario?"

def buscar_telefono():
	#enviar_android("buscar")
	return "el telefono empezara a sonar"

def silenciar():
	enviar_android('silencio')
	return 'El telefono esta siendo silenciado'

def transcripcionMusica(archivo,connection,client_address):
	global FASE
	global CANCION
	global MUSICA
	if client_address[0] == MINIBOB_ACTIVO:
		if FASE == 1:
			respuesta = pruebanuance.decode(archivo)
			sintildes = elimina_tildes(respuesta.decode('utf-8'))
			CANCION = sintildes.lower()
			print respuesta
			subprocess.call("rm " + archivo, shell=True)
			connection.sendall("se va a reproducir " + respuesta + ". Es correcto?")
			FASE = 2
		else:
			respuesta = transcribe.decode(archivo)
			
			print "ESTA ES LA CANCION " + CANCION
			if respuesta == None:
				subprocess.call("rm " + archivo, shell=True)
				connection.sendall("")
				return
			if type(respuesta) == str:
				palabras = respuesta.split()
				if len(palabras) == 1 and palabras[0].lower() == "si":
					MUSICA = False
					FASE = 1
					enviar_android('musica*' + CANCION + '.mp3')
					connection.sendall("")
				if len(palabras) == 1 and palabras[0].lower() == "no":
					connection.sendall("que cancion quiere reproducir")
					FASE = 1
			else:
				for x in xrange(0,len(respuesta)):
					if type(respuesta[x]) != str:
						continue
					palabras = respuesta[x].split()
					if len(palabras) == 1 and palabras[0].lower() == "si":
						MUSICA = False
						FASE = 1
						enviar_android('musica*' + CANCION + '.mp3')
						connection.sendall("")
						break
					if len(palabras) == 1 and palabras[0].lower() == "no":
						FASE = 1
						connection.sendall("que cancion quiere reproducir")
						break

def TrueorFalse(archivo):
	respuesta = transcribe.decode(archivo)
	if respuesta == None:
		return False
	if type(respuesta) == str:
		palabras = respuesta.split()
		if len(palabras) == 1 and palabras[0].lower() == "si":
			return True
		if len(palabras) == 1 and palabras[0].lower() == "no":
			return False
	else:
		for x in xrange(0,len(respuesta)):
			if type(respuesta[x]) != str:
				continue
			palabras = respuesta[x].split()
			if len(palabras) == 1 and palabras[0].lower() == "si":
				return True
			if len(palabras) == 1 and palabras[0].lower() == "no":
				return False
	return False

def transcripcionEmail(archivo,connection,client_address):
	global FASE_EMAIL
	global DESTINATARIO
	global ASUNTO
	global MENSAJE
	global EMAIL
	if client_address[0] == MINIBOB_ACTIVO:
		if FASE_EMAIL == 1:
			#recibes direccion de correo
			respuesta = pruebanuance.decode(archivo)
			sintildes = elimina_tildes(respuesta.decode('utf-8'))
			DESTINATARIO = sintildes.lower()
			print respuesta
			subprocess.call("rm " + archivo, shell=True)
			connection.sendall("el destinatario es  " + respuesta + ". Es correcto?")
			FASE_EMAIL = 2
		else:
			if FASE_EMAIL == 2:
				#recibes si es correcto o no la direccion
				if TrueorFalse(archivo):
					subprocess.call("rm " + archivo, shell=True)
					connection.sendall("cual es el asunto?")
					FASE_EMAIL = 3
				else:
					connection.sendall("cual es el destinatario?")
					FASE_EMAIL = 1
			else:
				if FASE_EMAIL == 3:
					respuesta = pruebanuance.decode(archivo)
					sintildes = elimina_tildes(respuesta.decode('utf-8'))
					ASUNTO = sintildes.lower()
					print respuesta
					subprocess.call("rm " + archivo, shell=True)
					connection.sendall("el asunto es  " + respuesta + ". Es correcto?")
					FASE_EMAIL = 4
				else:
					if FASE_EMAIL == 4:
						#recibes si es correcto o no el asunto
						if TrueorFalse(archivo):
							subprocess.call("rm " + archivo, shell=True)
							connection.sendall("cual es el mensaje?")
							FASE_EMAIL = 5
						else:
							connection.sendall("cual es el asunto?")
							FASE_EMAIL = 3
					else:
						if FASE_EMAIL == 5:
							#recibes el mensaje
							respuesta = pruebanuance.decode(archivo)
							sintildes = elimina_tildes(respuesta.decode('utf-8'))
							MENSAJE = sintildes.lower()
							print respuesta
							subprocess.call("rm " + archivo, shell=True)
							connection.sendall("el mensaje es  " + respuesta + ". Es correcto?")
							FASE_EMAIL = 6
						else:
							if FASE_EMAIL == 6:
								#recibes si es correcto o no el mensaje
								if TrueorFalse(archivo):
									subprocess.call("rm " + archivo, shell=True)
									connection.sendall("El mensaje sera enviado")
									FASE_EMAIL = 1
									EMAIL = False
									enviar_android('mail*' + DESTINATARIO + '*' + ASUNTO + '*' + MENSAJE)
								else:
									connection.sendall("cual es el asunto?")
									FASE_EMAIL = 5
		

operaciones = {'leer notificaciones': 'leer_notificaciones',
	'activar modo noche': activar_modo_noche,
	'desactivar modo noche' : desactivar_modo_noche,
	'emergencia' : emergencia,
	'enviar correo': 'enviar_correo',
	'donde esta mi telefono' : buscar_telefono,
	'reproducir musica' : musica,
	'silenciar telefono' : silenciar}

def obtenerTranscripcion(archivo,connection,client_address):
	#respuesta = speech_api.stt_google(archivo)
	res = ""
	global ACTIVE
	global MODONOCHE
	global MINIBOB_ACTIVO
	if not ACTIVE:
		respuesta = transcribe.decode(archivo)
		if respuesta == None:
			subprocess.call("rm " + archivo, shell=True)
			connection.sendall(res)
			return

		if type(respuesta) == str:
			palabras = respuesta.split()
			if len(palabras) == 1 and palabras[0].lower() == "emergencia":
				res = emergencia()
			if len(palabras) > 1:
				if "hola bob" in respuesta:
					res = "que puedo hacer por ti?"
					MINIBOB_ACTIVO = client_address[0]
					ACTIVE = True
		else:
			for x in xrange(0,len(respuesta)):
				if type(respuesta[x]) != str:
					continue
				palabras = respuesta[x].split()
				if len(palabras) == 1 and palabras[0].lower() == "emergencia":
					res = emergencia()
					break
				if len(palabras) > 1:
					if "hola bob" in respuesta[x]:
						res = "que puedo hacer por ti?"
						MINIBOB_ACTIVO = client_address[0]
						ACTIVE = True
						break
	else:
		if client_address[0] == MINIBOB_ACTIVO:
			print client_address
			respuesta = transcribe.decode(archivo)
			#respuesta = pruebanuance.decode(archivo)
			if respuesta == None:
				#subprocess.call("rm " + archivo, shell=True)
				connection.sendall(res)
				return
			if type(respuesta) == str:
				res = operaciones.get(respuesta.lower(),"")
				print res
			else:
				for x in xrange(0,len(respuesta)):
					if type(respuesta[x]) != str:
						continue
					print respuesta
					res = operaciones.get(respuesta[x].lower(),error)()
					if res != "":
						break
					palabras = respuesta[x].split()
					if "gracias bob" in respuesta[x]:
						res = "de nada senhor"
						ACTIVE = False
						break

	if MODONOCHE == True and res=="":
		res = "se ha detectado ruido"
	if MODONOCHE == False and ACTIVE == True and res == "":
		res = "no le he entendido"

	#subprocess.call("rm " + archivo, shell=True)
	connection.sendall(res)

def get_ip_address():
	s = socket(AF_INET, SOCK_DGRAM)
	return inet_ntoa(fcntl.ioctl(
		s.fileno(),
		0x8915,  # SIOCGIFADDR
		struct.pack('256s', IFNAME[:15])
	)[20:24])

def savefile(filename,audio):
	wave_file = wave.open(filename,"wb")
	wave_file.setnchannels(1)
	wave_file.setsampwidth(2)
	wave_file.setframerate(16000)
	wave_file.writeframes(audio)
	wave_file.close()

def hiloServer(connection,client_address):
	audio = ""
	try:
		f = open(str(client_address[0]) + '.wav','wb')
		while True:
			connection.settimeout(10)
			data = connection.recv(1024)
			#if data:
			f.write(data)
			#else:
			#	f.close()
			#	subprocess.call('mplayer archivo.mp3', shell=True)

	except timeout:
		f.close()
	# try:
	# 	while True:
	# 		connection.settimeout(1)
	# 		data = connection.recv(1024)
	# 		if data:
	# 			audio = audio + str(data)
	# 		else:
	# 			break
	# except timeout:
	#	savefile(str(client_address[0]) + ".wav", audio)
		if not MUSICA and not EMAIL:
			obtenerTranscripcion(str(client_address[0]) + ".wav",connection, client_address)
		else:
			if MUSICA:
				transcripcionMusica(str(client_address[0]) + ".wav",connection, client_address)
			else:
				transcripcionEmail(str(client_address[0]) + ".wav",connection, client_address)
		connection.close()


# def enviarBob

def procesarPeticiones(mensaje,client_address):
	if mensaje == 'home':
		global IP_ANDROID
		IP_ANDROID = client_address[0]
		return "discoverybob"

	cadena = mensaje.split('*')
	numPartes = len(cadena)

	if cadena[0] == 'notify' and cadena[1] == 'demo':
		for ip in IP_MINIBOBS:
			print ip
			sendToMinibob(ip,cadena[2])

	if numPartes == 2 and cadena[0] == 'app' and cadena[1] == 'connect':
		return "discoverybob"

	if cadena[0].lower() == 'notify' and numPartes >= 4:
		aplicacion = cadena[1]
		if aplicacion.lower() == 'whatsapp' or aplicacion.lower() == 'messenger' or aplicacion.lower() == 'sms' or aplicacion.lower() == 'tuit':
			remitente = cadena[2]
			contenido = cadena[3]
			if aplicacion.lower() == 'messenger':
				aplicacion = 'telegram'
			sendToMinibob(MINIBOB_ACTIVO,"tienes un " + aplicacion + "nuevo de " + remitente + ": "+ contenido)
		if aplicacion.lower() == 'mail':
			if cadena[2] == 'more':
				pass
			else:
				remitente = cadena[2]
				asunto = cadena[3]
				mensaje = cadena[4]
				sendToMinibob(IP_ANDROID,"tienes un mail nuevo de " + remitente + ": El asunto es " + asunto + " y el contenido: " + mensaje)

	if cadena[0].lower() == 'notify' and numPartes == 2:
		sendToMinibob(IP_ANDROID,"tienes una notificacion nueva de " + cadena[1])

def hiloandroid():
	host=get_ip_address()
    #host = "127.0.0.1"
	port = 9998
	s = socket(AF_INET,SOCK_STREAM)
	s.bind((host,port))
	s.listen(1)
   	addr = (host,port)
   	buf = 1024
   	while True:
   		print "esperando peticion"
	   	connection, client_address = s.accept()
	   	mensaje = ""
	   	try:
			while True:
				connection.settimeout(1)
				data = connection.recv(buf)
				if data:
					mensaje = mensaje + data
				else:
					break
		except timeout:
			pass
		finally:
			print mensaje
			respuesta = procesarPeticiones(mensaje,client_address)
			if respuesta == 'discoverybob':
				connection.sendall(respuesta)
			connection.close()

def hiloandroid2():
	host=get_ip_address()
    #host = "127.0.0.1"
	port = 9997
	s = socket(AF_INET,SOCK_STREAM)
	s.bind((host,port))
	s.listen(1)
   	addr = (host,port)
   	buf = 1024
   	while True:
   		print "esperando peticion"
	   	connection, client_address = s.accept()
	   	sock = socket(AF_INET,SOCK_STREAM)
	   	print "MINIBOB ACTIVO " + MINIBOB_ACTIVO
	   	sock.connect(MINIBOB_ACTIVO)
	   	f = open('archivo.mp3','wb')
	   	audio = ""
	   	# data = connection.recv(buf)
	   	# print data
		try:
			while True:
				connection.settimeout(1)
				data = connection.recv(buf)
				sock.sendall(data)
				if data:
					f.write(data)
					#udio = audio + str(data)
				else:
					f.close()
					# subprocess.call('mplayer ' + "archivo.mp3", shell=True)
					break
		except timeout:
			f.close()
			#savefile(str(client_address[0]) + ".wav", audio)
			#subprocess.call('mplayer ' + str(client_address[0]) + ".wav", shell=True)
			# print 'se va a reproducir'
			# subprocess.call('mplayer ' + "archivo.mp3", shell=True)
			#obtenerTranscripcion(str(client_address[0]) + ".wav",connection)
			connection.close()
			sock.close()



def main():
	#obtenemos las ip de android y minibobs
	global IP_ANDROID
	global IP_MINIBOBS
	if len(sys.argv) == 1:
		print "usage: ./server.py [IP_ANDROID] [IP_MINIBOBS]"
		sys.exit(0)

	IP_ANDROID = sys.argv[1]
	IP_MINIBOBS = []
	for i in range(2,(len(sys.argv))):
		IP_MINIBOBS.append(sys.argv[i])


	host=get_ip_address()
    #host = "127.0.0.1"
	port = 9999
	s = socket(AF_INET,SOCK_STREAM)
	s.bind((host,port))
	s.listen(1)
   	addr = (host,port)
   	print("server start o IP: " + host)
   	t = threading.Thread(target=hiloandroid2)
   	t.start()
   	t = threading.Thread(target=hiloandroid)
   	t.start()
   	while True:
	    buf=1024

	    audio=""

	    connection, client_address = s.accept()

	    t = threading.Thread(target=hiloServer,args=(connection,client_address))
	    t.start()
	    # try:
	    # 	while True:
	    # 		connection.settimeout(1)
	    # 		data = connection.recv(buf)
	    # 		if data:
	    # 			audio=audio+str(data)
	    # 		else:
	    # 			break
	    # except timeout:	
	    # 	savefile("prueba"+str(client_address[0])+".wav",audio)
	    # 	obtenerTranscripcion("prueba"+str(client_address[0])+".wav",connection)
	    # 	connection.close()



	    # data,addr = s.recvfrom(buf)
	    # print("receiving data")
	    # g.write(str(data))
	    # print data
	    # #name = data.strip();
	    # #f = open(name,'wb')

	    # data,addr = s.recvfrom(buf)
	    # try:
	    #     while(data):
	    #         g.write(str(data))
	    #         print data
	    #         s.settimeout(2)
	    #         data,addr = s.recvfrom(buf)
	    # except timeout:
	    # 	print "acabado"
	    #     g.close()
	    #     #obtenerTranscripcion(name,s,addr)
	    #     s.close()
            
if __name__ == "__main__":
    main()
