import socket
import commands
import time


miniBobList = ''

def generateBOB():
	print "generateBOB()"
	commands.getoutput('sudo sh generateBOB.sh')
	time.sleep(10)

def configLock():
    print "configLock()"
    miniBobList = miniBob.split('*')
    config = open("config.lock","a")
    for mB in miniBobList:
        if len(str(mB).split(':')) == 6:
            print mB
            config.write('\n' + mB)
    config.close()
    commands.getoutput('sudo reboot')

def searchMiniBobs():
	global miniBobList

	print "searchMiniBobs()"
	miniBobList = ''
	s = socket.socket()
	s.bind(('', 9990))
	s.listen(5)

	start = time.time()
	print 'Tiempo -> ' + str(start)

	while ((time.time() - start) < 120):
	    print time.time()
	    try:
			s.settimeout(40)
			c, addr = s.accept()
			data = c.recv(1024)
			print data
			if data.startsWith('newMiniBob*'):
			    miniBobList = miniBobList + '*' + data.split('*')[1]
			    c.send('config*wifi*' + ssid + '*' + password)

			c.close()
	    except Exception:
	        print 'timeout'
            c.close()
	print 'Finalizada la busqueda de miniBOBs -> *' + miniBobList


if __name__ == '__main__':

    s=socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
    s.bind(('',9995))
    print 'Iniciando servidor para sincronizar nuevos miniBOBs'
    while True:
        data,addr=s.recvfrom(1024)
        print('Address:',addr,'Data:',data)

        if data.startswith('app*connect'):
            s.sendto(str('discoverybob'),addr)

        if data.startswith('newMiniBob'):
            generateBOB()
            searchMiniBobs()
            configLock()
