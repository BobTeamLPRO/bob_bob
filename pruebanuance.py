#-*- coding: utf-8 -*-
# @author: Bob Team (Victor Corchero)
# @version: 0.1
# @notes: unoficial version developed by Victor Corchero for Python 2.7
import urllib2
import os
import sys
import json
import httplib
import wave
import subprocess
import codecs
from time import sleep
import base64

ISPEECH_URL = "https://dictation.nuancemobility.net:443/NMDPAsrCmdServlet/dictation?appId=NMDPTRIAL_victorcorchero_outlook_com20160222070656&appKey=d6d0ba838dc8bb23dca6ad89947ef2019cd9c99c67ac523fa1b839bf5f3bb6350b70b9983974bb151a689faee691b0826a791fd1257111c7294a8689853cbe90&id=C4461956B60B";


#envia a iSpeech el archivo (.wav) que se pasa por parametro
# y devuelve posibles frases (la mas probable siempre es la 1)
def decode(filename):

    print "entrando en decode"

    hrs = {'Content-type': 'audio/x-wav;codec=pcm;bit=16;rate=16000','Accept': 'application/xml','Accept-Language': 'es_es'}

    #content = "apikey=" + apikey + "&action=" + action + "&freeform=" + freeform + "&content-type=" + content_type + "&output=" + output + "&locale=" + locale + "&audio=" + base64_encode(file_get_contents(filename))
    
    f = open(filename, 'rb')
    content = f.read()
    f.close()

    req = urllib2.Request(ISPEECH_URL, data=content, headers=hrs)
    print "Sending request to NUANCE TTS"
    try:
        p = urllib2.urlopen(req)
    except urllib2.HTTPError, err:
        print "error"
        return str(err.code)
    response = p.read()
    rep = response.split('\n')
    for i in xrange(0,len(rep)):
        print rep[i]
    return rep[0]
    


# # @author: Bob Team (Victor Corchero)
# # @version: 0.1
# # @notes: unoficial version developed by Victor Corchero for Python 2.7
# import urllib2
# import os
# import sys
# import json
# import httplib
# import wave
# import subprocess
# import codecs
# from time import sleep
# import base64

# ISPEECH_URL = "https://tts.nuancemobility.net:443/NMDPTTSCmdServlet/tts?appId=NMDPTRIAL_victorcorchero_outlook_com20160222070656&appKey=d6d0ba838dc8bb23dca6ad89947ef2019cd9c99c67ac523fa1b839bf5f3bb6350b70b9983974bb151a689faee691b0826a791fd1257111c7294a8689853cbe90&id=57349abd2390&ttsLang=es_es&voice=jorge";


# #envia a iSpeech el archivo (.wav) que se pasa por parametro
# # y devuelve posibles frases (la mas probable siempre es la 1)
# def decode(filename):

#     print "entrando en decode"

#     hrs = {'Content-type': 'text/plain','Accept': 'audio/x-wav'} 

#     #content = "apikey=" + apikey + "&action=" + action + "&freeform=" + freeform + "&content-type=" + content_type + "&output=" + output + "&locale=" + locale + "&audio=" + base64_encode(file_get_contents(filename))
#     content = "quedamos mañana a las 8"
#     print content
#     req = urllib2.Request(ISPEECH_URL, data=content, headers=hrs)
#     print "Sending request to iSpeech TTS"
#     p = urllib2.urlopen(req)
#     response = p.read()
    
#     f = open("audio.pcm",'wb')
#     f.write(response)
#     f.close()


# def file_get_contents(path):
#         try:
#             f = open(path, "rb")
#             try:
#                 result = f.read()
#             finally:
#                 f.close()
#                 return result
#         except IOError:
#             print("Error reading file")
# def base64_encode(var):
#         return base64.b64encode(var).replace(b'\n',b'')

# if __name__=='__main__':
#     decode("asd")
