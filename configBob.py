import sys
import commands
import time
import socket
import os.path
import subprocess
import threading

macApp = ''
ssid = ''
password = ''
miniBobList = ''

def demoBOB():
	subprocess.call('sudo python discoveryBobDemo.py',shell=True)

def getIPAddress():
	commands.getoutput('hostname -I').split()[0]

def launchServer():
	print "launchServer()"
	commands.getoutput('nmap -sn ' + str(getIPAddress()) + '/24')
	time.sleep(5)
	launchCommand = 'python server.py '
	arp = commands.getoutput('arp -a').split('\n')
	config = open('config.lock','r').readlines()
	for mac in config:
		mac = mac.strip()
		if ':' in mac:
			for ip in arp:
				if mac in ip:
					launchCommand = launchCommand + ' ' + ip.split('(')[1].split(')')[0]
	t = threading.Thread(target=demoBOB)
	t.start()
	print launchCommand
	subprocess.call(launchCommand,shell=True)


def searchMiniBobs():
	global miniBobList

	print "searchMiniBobs()"
	miniBobList = ''
	s = socket.socket()
	s.bind(('', 9995))
	s.listen(5)

	start = time.time()
	print 'Tiempo -> ' + str(start)

	while ((time.time() - start) < 120):
	    print time.time()
	    try:
			s.settimeout(20)
			c, addr = s.accept()
			data = c.recv(1024)
			print data
			if data.startsWith('newMiniBob*'):
			    miniBobList = miniBobList + '*' + data.split('*')[1]
			    c.send('config*wifi*' + ssid + '*' + password)

			c.close()
	    except Exception:
	        print 'timeout'
	print 'Finalizada la busqueda de miniBOBs -> *' + miniBobList

def configLock(miniBob):
    print "configLock()"
    if 'null' in miniBob:
        config = open("config.lock","w")
        config.write(ssid + '\n')
        config.write(password + '\n')
        config.write(macApp)
        config.close()

    else:
        miniBobList = miniBob.split('*')
        config = open("config.lock","a")
        for mB in miniBobList:
			if len(str(mB).split(':')) == 6:
				print mB
				config.write('\n' + mB)
        config.close()
	print 'sudo reboot'
	#commands.getoutput('sudo reboot')

def appConnect(ssids):
    print "appConnect()"
    global ssid
    global password
    global macApp

    s = socket.socket()
    s.bind(('', 9994))
    s.listen(5)

    print "Conectando con la app 1"
    c, addr = s.accept()
    data = c.recv(1024)

    if data.startswith('app*connect*'):
        print "Conectando con la app 2"
        macApp = data.split('*')[2]
        c.send(str(ssids))

    data = c.recv(1024)
    if data.startswith('config*wifi*'):
        print "Conectando con la app 3"
        ssid = data.split('*')[2]
        password = data.split('*')[3]

    c.close()

def generateBOB():
	print "generateBOB()"
	commands.getoutput('sudo sh generateBOB.sh')
	time.sleep(10)

def configWifi():
    print "configWifi() " + ssid + ' ' + password
    wpa = open("interfaces","w")

    wpa.write("source-directory /etc/network/interfaces.d \n")

    wpa.write("auto lo \n")
    wpa.write("iface lo inet loopback \n")
    wpa.write("iface eth0 inet manual \n")
    wpa.write("allow-hotplug wlan0 \n")
    wpa.write("auto wlan0 \n")
    wpa.write("iface wlan0 inet dhcp \n")
    wpa.write("wpa-ssid \""+ ssid +"\" \n")
    wpa.write("wpa-psk \""+ password +"\" \n")

    wpa.close()


    commands.getoutput('sudo service hostapd stop')
    commands.getoutput('sudo service dnsmasq stop')
    commands.getoutput('sudo ifconfig wlan0 down')
    commands.getoutput('sudo cp /home/pi/BOB/interfaces /etc/network/interfaces')
    commands.getoutput('sudo ifconfig wlan0 up')


def getWifiNetworks():
    print "getWifiNetworks()"
    commands.getoutput('iwlist wlan0 scan | egrep -w "ESSID|WPA2" > wifiNetworks.txt')
    wifiNetworks = open("wifiNetworks.txt","r")
    print wifiNetworks
    wifis = "*"
    conf = False

    for line in wifiNetworks:
        if 'ESSID:' in line:
            if (conf):
                wifis = wifis + "OPEN*"
                conf = False

            if 'ESSID:""' in line:
                continue

            else:
                wifis = wifis + line.split('"')[1] + "*"
                conf = True
        else:
            if ('IE: IEEE 802.11i/WPA2 Version 1' in line and conf):
                wifis = wifis + "WPA2*"
                conf = False
    return wifis

def getMAC(interface):
    print "getMAC()"
    try:
        str = open('/sys/class/net/' + interface + '/address').read()
    except:
        str = "00:00:00:00:00:00"
    return str[0:17]


if __name__ == '__main__':
	if os.path.exists('config.lock'):
		if len(open('config.lock','r').readlines()) < 4 :
			print 'APP configurada, faltan miniBOBs'
			if (commands.getstatusoutput('ping -c 10 8.8.8.8')[0] == 0):
				print 'ping 1'
				generateBOB()
				searchMiniBobs()
				configLock(miniBobList)
			else:
				time.sleep(10)
				if (commands.getstatusoutput('ping -c 10 8.8.8.8')[0] == 0):
					print 'ping 2'
					generateBOB()
					mbs = searchMiniBobs()
					configLock(mbs)
				else:
					print 'PROBLEMAS'
					#sudo reboot
		else:
			launchServer()

	else:
		MAC = getMAC("wlan0")
		ssids = "config*" + MAC + getWifiNetworks()
		print "Contactando con la app " + ssids
		generateBOB()
		appConnect(ssids)
		configWifi()
		configLock('null')
