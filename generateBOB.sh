#!/bin/bash

killall hostapd
killall dnsmasq
ifconfig wlan0 down
ifconfig wlan0 10.0.0.1 netmask 255.255.255.0 up
iwconfig wlan0 power off
service dnsmasq restart
service hostapd start
hostapd -B /etc/hostapd/hostapd.conf & > /dev/null 2>&1
